from selenium.webdriver.common.by import By


class PageTencentHome():
    def __init__(self,dirver):
        self.driver =dirver
        self.loc_ele_search_box=(By.XPATH,'//android.widget.FrameLayout[@resource-id="com.tencent.news:id/aqs"]')
        self.loc_ele_channel_button=(By.XPATH,'//android.widget.ImageView[@resource-id="com.tencent.news:id/qz"]')


    '''点击搜索框'''
    def ele_search_box(self):
        self.driver.find_element(*self.loc_ele_search_box).click()
    '''点击频道按钮'''
    def ele_channel_button(self):
        self.driver.find_element(*self.loc_ele_channel_button).click()
    '''进行页面刷新'''
    def ele_Screen_refresh(self,sx,sy,ex,ey,time):
        self.driver.swipe(sx,sy,ex,ey,time)

    '''创建聚合函数'''
    def home(self,sx,sy,ex,ey,time):
        self.ele_search_box()
        self.ele_channel_button()
        self.ele_Screen_refresh(sx,sy,ex,ey,time)
