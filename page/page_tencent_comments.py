from selenium.webdriver.common.by import By


class PageTencentComments():
    def __init__(self,driver):
        self.driver = driver
        self.loc_ele_tencent_news=(By.XPATH,'//android.widget.TextView[@text=\"共谋人民幸福，百年大党这样选择\"]')
        self.loc_ele_tencent_search=(By.XPATH,'//android.widget.TextView[@resource-id=\"com.tencent.news:id/qv\"]')
        self.loc_ele_tencent_send=(By.XPATH,'//android.widget.EditText[@resource-id=\"com.tencent.news:id/azj\"]')
        self.loc_ele_tencent_identify=(By.XPATH,'//android.widget.TextView[@resource-id=\"com.tencent.news:id/r5\"]')




    '''点击顶置新闻'''
    def ele_tencent_news(self):
        self.driver.find_element().click()
    '''点击输入框'''
    def ele_tencent_search(self):
        self.driver.find_element().click()
    '''输入评论'''
    def ele_tencent_send(self,word):
        self.driver.find_element().send_keys(word)
    '''点击【发布】按钮'''
    def ele_tencent_identify(self):
        self.driver.find_element().click()
    '''创建聚合函数'''
    def comments(self,word):
        self.ele_tencent_news()
        self.ele_tencent_search()
        self.ele_tencent_send(word)
        self.ele_tencent_identify()



