from selenium.webdriver.common.by import By


class PageTencentSearch():
    def __init__(self,driver):
        self.driver = driver
        self.loc_ele_tencent_empty=(By.XPATH,'//android.widget.EditText[@resource-id="com.tencent.news:id/cdw"]')
        self.loc_ele_tencent_send=(By.XPATH,'//android.widget.EditText[@resource-id="com.tencent.news:id/cdw"]')


    '''清空输入框内容'''
    def ele_tencent_empty(self):
        self.driver.find_element().clear()

    '''搜索框内输入内容'''
    def ele_tencent_send(self,content):
        self.driver.find_element().send_keys(content)
    '''按下搜索键搜索'''
    def ele_tencent_enter(self,num):
        self.driver.press_keycode(num)

    def search(self,content,num):
        self.ele_tencent_empty()
        self.ele_tencent_send(content)
        self.ele_tencent_enter(num)

