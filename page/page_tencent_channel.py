import time

from selenium.webdriver.common.by import By


class PageTencentChannel():
    def __init__(self,driver):
        self.driver = driver
        self.loc1_ele_tencent_drag=(By.XPATH, '//android.widget.TextView[@text=\"小视频\"]')
        self.loc2_ele_tencent_drag=(By.XPATH, '//android.widget.TextView[@text=\"抗疫\"]')
        self.loc_ele_tencent_choice=(By.XPATH,'//android.widget.TextView[@text=\"成都\"]')
        self.loc_ele_tencent_switch=(By.XPATH,'//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.View[2]')


    '''对某个模块进行拖拽'''
    def ele_tencent_drag(self):
        eo1 = self.driver.find_element(*self.loc1_ele_tencent_drag)
        eo2 = self.driver.find_element(*self.loc2_ele_tencent_drag)
        self.driver.drag_and_drop(eo1, eo2)

    time.sleep(3)

    '''选择频道为成都'''
    def ele_tencent_choice(self):
        self.driver.find_element(*self.loc_ele_tencent_choice).click()

    '''切换城市'''
    def ele_tencent_switch(self):
        self.driver.find_element(*self.loc_ele_tencent_switch).click()

    def channel(self):
        self.ele_tencent_drag()
        self.ele_tencent_choice()
        self.ele_tencent_switch()


