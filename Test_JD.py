#引入unittest 类
import unittest

# 引入webdriver模块
from selenium import webdriver

# 引入time模块
import time

# 引入ActionChains类
from selenium.webdriver.common.action_chains import ActionChains

#引入keys类
from selenium.webdriver.common.keys import Keys

class JdShopping(unittest.TestCase):
    def test_disk(self):
        # 实例化driver对象
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问京东
        driver.get("http://www.JD.com")

        # 打开京东页面，通过悬浮找到：【电脑/移动硬盘】进入列表进行关键词搜索操作
        loca = driver.find_element_by_xpath('//div[@id="J_cate"]/ul/li[3]/a[1]')
        action = ActionChains(driver)
        action.move_to_element(loca).perform()
        time.sleep(3)
        driver.find_element_by_xpath('//div[@id="cate_item3"]/div[1]/div[2]/dl[3]/dd/a[7]').click()

        # 获取当前窗口的所有句柄
        handles = driver.window_handles
        # 跳转值当前最新窗口
        driver.switch_to.window(handles[-1])

        # 进行条件筛选，三个条件，
        driver.find_element_by_xpath('//div[@id="J_selector"]/div[2]/div/div[2]/div[1]/ul/li[1]/a').click()
        driver.find_element_by_xpath('//div[@id="J_selector"]/div[3]/div/div[2]/div[1]/ul/li[2]/a').click()
        driver.find_element_by_xpath('//div[@id="J_selector"]/div[3]/div/div[2]/div[1]/ul/li[1]/a').click()
        driver.find_element_by_xpath('//div[@id="J_goodsList"]/ul/li[2]/div/div[1]/a/img').click()

        # 获取当前窗口的所有句柄
        handles = driver.window_handles
        # 跳转值当前最新窗口
        driver.switch_to.window(handles[-1])
        time.sleep(6)
        driver.find_element_by_xpath('//a[@id="InitCartUrl"]').click()
        print("添加购物车成功")
        time.sleep(2)

        # 关闭当前所有窗口，并关闭浏览器驱动
        driver.quit()

    def test_registered(self):
        driver = webdriver.Chrome()

        driver.get('https://reg.jd.com/p/regPage?ignore=1')

        driver.maximize_window()

        driver.find_element_by_xpath('/html/body/div[4]/div[2]/div/div[2]/button').click()
        driver.find_element_by_id('form-phone').send_keys("18982756243")
        time.sleep(2)
        driver.find_element_by_id('form-phone').send_keys(Keys.CONTROL, "a")
        time.sleep(2)
        driver.find_element_by_id('form-phone').send_keys(Keys.CONTROL, "x")
        time.sleep(2)
        driver.find_element_by_id('form-phone').send_keys(Keys.CONTROL, "v")
        time.sleep(2)
        # driver.find_element_by_id('form-phone').send_keys(Keys.CONTROL,"a")
        time.sleep(2)
        for i in range(11):
            driver.find_element_by_id('form-phone').send_keys(Keys.BACK_SPACE)
            time.sleep(1)
        time.sleep(2)
        driver.find_element_by_xpath('//div[@id="step1-wrap"]/div[2]/div[1]').click()
        time.sleep(2)

        # 定位起始位置元素
        source = driver.find_element_by_xpath('//div[@id="slideAuthCode"]/div/div[2]/div[3]')
        # 定位结束位置元素
        target = driver.find_element_by_class_name('JDJRV-slide-right')
        # 实例化对象
        action = ActionChains(driver)
        action.drag_and_drop(source, target).perform()
        time.sleep(2)
        # 鼠标拖动事件
        action.drag_and_drop_by_offset(source, 168, 0).perform()
        print("注册事件成功")
        time.sleep(2)

        # 关闭当前所有窗口，并关闭浏览器驱动
        driver.quit()
    def test_add(self):
        # 实例化driver对象
        driver = webdriver.Chrome()
        # 最大化窗口
        driver.maximize_window()
        # 调用get方法访问京东
        driver.get("http://www.JD.com")
        # 点击话费充值，选择充值金额，点击充值按钮
        driver.find_element_by_xpath('//div[@id="J_service"]/div[1]/ul/li[1]/a/span').click()

        handles = driver.window_handles

        driver.switch_to.window(handles[-1])

        time.sleep(2)

        driver.switch_to.frame("fast-cziframe")  # 跳转至frame(通过id值)

        print(driver.title)  # 打印标题

        time.sleep(3)

        driver.find_element_by_xpath('//div[@id="phoneitem"]/div/input').send_keys("18982756243")

        driver.find_element_by_xpath('//div[@class="productList"]/ul/li[8]').click()

        # 跳回主文档，也就是最外层
        driver.switch_to.default_content()

        time.sleep(2)

        driver.find_element_by_xpath('//div[@class="content"]/div/ul/li[2]').click()

        # 进入id名为：flowiframe的frame
        driver.switch_to.frame("flowiframe")

        time.sleep(3)

        driver.find_element_by_xpath('//div[@id="phoneitem"]/div/input').send_keys("18982756143")

        driver.find_element_by_xpath('//div[@id="flowItem"]/div/ul/li[1]').click()

        driver.find_element_by_xpath('//div[@id="priceItem"]/ul/li[1]').click()

        time.sleep(3)

        # driver.find_element_by_class_name("submit").click()

        # 跳回主文档，也就是最外层
        driver.switch_to.default_content()

        # 点击话费充值
        driver.find_element_by_xpath('//div[@class="content"]/div/ul/li[1]').click()
        # 点多人充值
        driver.find_element_by_xpath('/html/body/div[5]/div/div[2]/div[1]/div[1]/ul/li[2]').click()
        # 跳转至frame，根据id值确定
        driver.switch_to.frame("many-cziframe")
        # 输入号码，选择金额，确认充值
        driver.find_element_by_xpath('//div[@id="phoneitem"]/ul/li[1]/div[1]/input').send_keys("18982756243")

        driver.find_element_by_xpath('//div[@id="phoneitem"]/ul/li[1]/div[2]/i/s').click()

        driver.find_element_by_xpath('//div[@id="phoneitem"]/ul/li[2]/div[1]/input').send_keys("13208303285")

        driver.find_element_by_xpath('//div[@id="phoneitem"]/ul/li[2]/div[2]/i/s').click()

        driver.find_element_by_class_name('submit').click()
        print("话费充值成功")

        # 退出所有窗口，关闭浏览器驱动
        driver.quit()

    if __name__ == '__main__':
        unittest.main()