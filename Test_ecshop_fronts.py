#引入unittest
import unittest

#引入time类
import time

#引入webdriver
from selenium import webdriver


class EcshopFronts(unittest.TestCase):

    def test_select(self):
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问ECshop
        driver.get("http://192.168.1.241/hdshop")

        # 登录账号
        time.sleep(2)
        driver.find_element_by_link_text('登录').click()
        driver.find_element_by_name("username").send_keys("mark8")
        driver.find_element_by_name("password").send_keys("123456")
        time.sleep(3)
        driver.find_element_by_xpath('//input[@id="remember"]').click()
        driver.find_element_by_css_selector('.us_Submit').click()

        time.sleep(3)

        # 搜索商品，选择分类，选择商品，查看商品详情
        driver.find_element_by_xpath('//input[@id="keyword"]').clear()
        time.sleep(2)
        driver.find_element_by_xpath('//input[@id="keyword"]').send_keys("钻石")
        driver.find_element_by_xpath('//form[@id="searchForm"]/span[2]/input').click()
        print("搜索成功")

        time.sleep(2)
        driver.quit()


    def test_add_shopping(self):
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问ECshop
        driver.get("http://192.168.1.241/hdshop")

        # 登录账号
        time.sleep(2)
        driver.find_element_by_link_text('登录').click()
        driver.find_element_by_name("username").send_keys("mark8")
        driver.find_element_by_name("password").send_keys("123456")
        time.sleep(3)
        driver.find_element_by_xpath('//input[@id="remember"]').click()
        driver.find_element_by_css_selector('.us_Submit').click()

        time.sleep(3)

        # 搜索商品，选择分类，选择商品，进入商品详情，加入购物车
        driver.find_element_by_xpath('//input[@id="keyword"]').clear()
        time.sleep(2)
        driver.find_element_by_xpath('//input[@id="keyword"]').send_keys("钻石")
        driver.find_element_by_xpath('//form[@id="searchForm"]/span[2]/input').click()
        time.sleep(2)
        driver.find_element_by_xpath('//form[@id="compareForm"]/div/div[1]/a/img').click()
        driver.find_element_by_xpath('//form[@id="ECS_FORMBUY"]/ul[3]/li[2]/a/img').click()
        print("加入购物车成功")
        time.sleep(2)

        # 关闭所有窗口并关闭浏览器驱动
        driver.quit()

    def test_settlement(self):
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问ECshop
        driver.get("http://192.168.1.241/hdshop")

        # 登录账号
        time.sleep(2)
        driver.find_element_by_link_text('登录').click()
        driver.find_element_by_name("username").send_keys("mark8")
        driver.find_element_by_name("password").send_keys("123456")
        time.sleep(3)
        driver.find_element_by_xpath('//input[@id="remember"]').click()
        driver.find_element_by_css_selector('.us_Submit').click()

        time.sleep(3)

        # 搜索商品，选择分类，选择商品，进入商品详情，加入购物车
        driver.find_element_by_xpath('//input[@id="keyword"]').clear()
        time.sleep(2)
        driver.find_element_by_xpath('//input[@id="keyword"]').send_keys("钻石")
        driver.find_element_by_xpath('//form[@id="searchForm"]/span[2]/input').click()
        time.sleep(2)
        driver.find_element_by_xpath('//form[@id="compareForm"]/div/div[1]/a/img').click()
        driver.find_element_by_xpath('//form[@id="ECS_FORMBUY"]/ul[3]/li[2]/a/img').click()
        time.sleep(2)

        # 结算，填入结算信息，提交订单
        driver.find_element_by_xpath('/html/body/div[7]/div[1]/table/tbody/tr/td[2]/a/img').click()
        driver.find_element_by_xpath('//select[@id="selCountries_0"]').click()
        driver.find_element_by_xpath('//select[@id="selProvinces_0"]').click()
        time.sleep(3)
        driver.find_element_by_xpath('//select[@name="province"]/option[24]').click()
        driver.find_element_by_xpath('//select[@id="selCities_0"]').click()
        time.sleep(3)
        driver.find_element_by_xpath('//select[@name="city"]/option[2]').click()

        driver.find_element_by_xpath('//select[@id="selDistricts_0"]').click()
        driver.find_element_by_xpath('//select[@name="district"]/option[2]').click()

        driver.find_element_by_xpath('//input[@id="consignee_0"]').send_keys("刘哈哈")

        driver.find_element_by_xpath('//input[@id="address_0"]').send_keys("四川省成都市锦江区东方广场C座")
        driver.find_element_by_xpath('//input[@id="tel_0"]').send_keys("18982756243")
        # 填写配送信息
        time.sleep(2)
        driver.find_element_by_xpath('//input[@id="email_0"]').send_keys("592785414@qq.com")
        driver.find_element_by_css_selector(".bnt_blue_2").click()

        driver.find_element_by_xpath('//table[@id="shippingTable"]/tbody/tr[3]/td[1]/input').click()
        driver.find_element_by_xpath('//table[@id="paymentTable"]/tbody/tr[4]/td[1]/input').click()

        driver.find_element_by_xpath('//form[@id="theForm"]/div[11]/div[2]/input[1]').click()

        print("结算成功")
        time.sleep(3)
        driver.quit()

    def test_order(self):
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问ECshop
        driver.get("http://192.168.1.241/hdshop")

        # 登录账号
        time.sleep(2)
        driver.find_element_by_link_text('登录').click()
        driver.find_element_by_name("username").send_keys("mark8")
        driver.find_element_by_name("password").send_keys("123456")
        time.sleep(3)
        driver.find_element_by_xpath('//input[@id="remember"]').click()
        driver.find_element_by_css_selector('.us_Submit').click()
        time.sleep(3)

        # 查看订单信息
        driver.find_element_by_xpath('/html/body/div[2]/div[2]/ul/li[2]/a').click()
        handles = driver.window_handles  # 获取所有窗口的句柄
        driver.switch_to.window(handles[-1])  # 跳转至最新的窗口
        driver.find_element_by_xpath('/html/body/div[7]/div[1]/div/div/div/div/a[3]').click()
        print("查看订单成功")

        # 退出所有窗口，关闭浏览器驱动
        driver.quit()


if __name__ == '__main__':
    unittest.main()