#引入unittest
import unittest

# 引入webdriver模块（类）
from selenium import webdriver
# 引入time模块
import time

class FrontBack(unittest.TestCase):
class FrontBack(unittest.TestCase):
    def test_selects(self):

        # 实例化driver对象
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问ECshop
        driver.get("http://192.168.1.241/hdshop")

        # 登录账号
        time.sleep(2)
        driver.find_element_by_link_text('登录').click()
        driver.find_element_by_name("username").send_keys("mark8")
        driver.find_element_by_name("password").send_keys("123456")
        time.sleep(3)
        driver.find_element_by_xpath('//input[@id="remember"]').click()
        driver.find_element_by_css_selector('.us_Submit').click()

        time.sleep(3)

        # 查看订单信息
        driver.find_element_by_xpath('/html/body/div[2]/div[2]/ul/li[2]/a').click()
        handles = driver.window_handles  # 获取所有窗口的句柄
        driver.switch_to.window(handles[-1])  # 跳转至最新的窗口
        driver.find_element_by_xpath('/html/body/div[7]/div[1]/div/div/div/div/a[3]').click()

        # 获取订单号
        x = driver.find_element_by_xpath('/html/body/div[8]/div[2]/div/div/div/table/tbody/tr[2]/td[1]/a').text

        # 进入后台，查询对应订单
        # 登录后台会员
        driver.get("http://192.168.1.241/hdshop/admin/")
        driver.find_element_by_xpath("//input[@name='username']").send_keys("admin")
        driver.find_element_by_xpath("//input[@name='password']").send_keys("hdxy2018")
        driver.find_element_by_css_selector(".btn-a").click()
        time.sleep(3)
        driver.switch_to.frame("menu-frame")  # 跳转至该模块的frame
        time.sleep(2)
        driver.find_element_by_xpath('//ul[@id="menu-ul"]/li[3]').click()

        driver.find_element_by_xpath('//ul[@id="menu-ul"]/li[3]/ul/li[2]/a').click()

        driver.switch_to.default_content()
        driver.switch_to.frame("main-frame")
        time.sleep(3)

        driver.find_element_by_xpath('//input[@id="order_sn"]').send_keys(x)
        time.sleep(2)
        driver.find_element_by_xpath('//input[@id="query"]').click()
        print("查询订单成功")
        time.sleep(6)

        # 关闭当前所有窗口，并关闭浏览器驱动
        driver.quit()

if __name__ == '__main__':
    unittest.main()