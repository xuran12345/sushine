'''引入需要调用的类和方法'''
import time
import unittest
from appium import webdriver
from page.page_tencent_comments import PageTencentComments

'''进入首页点击置顶新闻'''
'''点击评论输入框'''
'''输入评论内容'''
'''点击评论按钮'''


desired_capabilities ={
    'platformName':'Android',
    'deviceName':'127.0.0.1:62001',   #模拟器的名称
    'platformVersion':'7.1.2',   #设备的系统平台：Android、IOS
    'appPackage':'com.tencent.news',  #设备系统(模拟器)的Android版本号
    'appActivity':'com.tencent.news.activity.SplashActivity',
    'unicodeKeyboard':True,  #将输入内容设置为中文
    'resetKeyboard':True, #重置输入法为初始状态
    'noReset':True, #设置后APP不会回到初始化状态;避免重启
    'newCommandTimeout':60 #命令等待超时时间


}
class TestTencentComments(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_capabilities)
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()


    def test_comments(self):
        '''进入首页点击置顶新闻，并评论，发表评论'''
        word="z祖国繁荣昌盛"
        PageTencentComments(self.driver).comments(word)

    time.sleep(3)



if __name__ == '__main__':
    unittest.main()