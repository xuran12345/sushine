import  unittest

from BeautifulReport import BeautifulReport

discover = unittest.defaultTestLoader.discover(start_dir='./',
                                               pattern='test*.py',
                                               top_level_dir=None)

BeautifulReport(discover).report(filename='报告最新.html',
                                 log_path='./',
                                 description='报告详情')