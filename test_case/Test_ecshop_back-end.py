#引入unittest
import unittest
#引入webdriwer
from selenium import  webdriver

#引入time
import  time


# 引入Select类
from selenium.webdriver.support.select import Select

class EcshopBack(unittest.TestCase):

    def test_lgoinback(self):
        import time

        driver = webdriver.Chrome()

        driver.maximize_window()

        driver.get("http://192.168.1.241/hdshop/admin/")

        driver.find_element_by_xpath("//input[@name='username']").send_keys("admin")

        driver.find_element_by_xpath("//input[@name='password']").send_keys("hdxy2018")

        driver.find_element_by_css_selector(".btn-a").click()
        print("后台登录成功")

        time.sleep(3)

        driver.quit()

    def test_addgoods(self):


        # 实例化driver对象
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问12306
        driver.get("http://192.168.1.241/hdshop/admin/")

        # 登录后台会员
        driver.find_element_by_xpath("//input[@name='username']").send_keys("admin")

        driver.find_element_by_xpath("//input[@name='password']").send_keys("hdxy2018")

        driver.find_element_by_css_selector(".btn-a").click()
        time.sleep(3)

        # 进入商品管理，添加商品
        driver.switch_to.frame("menu-frame")  # 跳转至该模块的frame
        time.sleep(2)
        driver.find_element_by_xpath('//ul[@id="menu-ul"]/li[1]').click()
        driver.find_element_by_xpath('//ul[@id="menu-ul"]/li[1]/ul/li[2]/a').click()

        driver.switch_to.default_content()  # 跳出最外层
        driver.switch_to.frame('main-frame')

        # 输入商品名称
        driver.find_element_by_xpath('//input[@name="goods_name"]').send_keys("国王的新衣4")
        # 通过引入Select类来处理下拉框选择对象（选择商品分类）
        cat = driver.find_element_by_name("cat_id")
        time.sleep(2)
        select = Select(cat)
        time.sleep(2)
        select.select_by_value("160")
        time.sleep(2)

        # 选择品牌
        brand = driver.find_element_by_name("brand_id")
        time.sleep(2)
        select = Select(brand)
        time.sleep(2)
        select.select_by_value("36")
        time.sleep(2)

        # 添加商品价格
        driver.find_element_by_xpath('//table[@id="general-table"]/tbody/tr[6]/td[2]/input[1]').clear()
        driver.find_element_by_xpath('//table[@id="general-table"]/tbody/tr[6]/td[2]/input[1]').send_keys("99")
        time.sleep(2)

        # 输入商品优惠信息
        driver.find_element_by_xpath('//table[@id="tbody-volume"]/tbody/tr/td/input[1]').send_keys("2")
        driver.find_element_by_xpath('//table[@id="tbody-volume"]/tbody/tr/td/input[1]').send_keys("88")
        time.sleep(2)

        # 处理日期控件(开始日期)移除只读功能，可以通过输入内容快速实现日期的更改，精简了操作；此情况不仅适用于日期控件；还适用于
        js = "document.getElementById('promote_start_date').removeAttribute('readonly')"
        driver.execute_script(js)
        driver.find_element_by_id('promote_start_date').clear()
        driver.find_element_by_id('promote_start_date').send_keys('2021-6-30')

        # 处理日期控件（结束日期）
        js1 = "document.getElementById('promote_end_date').removeAttribute('readonly')"
        driver.execute_script(js1)
        driver.find_element_by_id('promote_end_date').clear()
        driver.find_element_by_id('promote_end_date').send_keys('20221-7-12')

        # 上传图片
        driver.find_element_by_name('goods_img').send_keys(r" C:\Users\旧南亭\Desktop\gouzi.jpg")

        time.sleep(2)
        # 确认添加商品
        driver.find_element_by_xpath('//div[@id="tabbody-div"]/form/div/input[2]').click()
        print("添加商品成功")
        time.sleep(3)

    def test_deletegoods(self):

        # 实例化driver对象
        driver = webdriver.Chrome()
        # 窗口最大化
        driver.maximize_window()
        # 调用get方法访问ECshop后台
        driver.get("http://192.168.1.241/hdshop/admin/")

        # 登录后台会员
        driver.find_element_by_xpath("//input[@name='username']").send_keys("admin")
        driver.find_element_by_xpath("//input[@name='password']").send_keys("hdxy2018")
        driver.find_element_by_css_selector(".btn-a").click()
        time.sleep(3)

        driver.switch_to.frame("menu-frame")  # 跳转至该模块的frame
        time.sleep(2)
        driver.find_element_by_xpath('//ul[@id="menu-ul"]/li[1]').click()
        driver.find_element_by_xpath('//ul[@id="menu-ul"]/li[1]/ul/li[1]/a').click()

        driver.switch_to.default_content()  # 跳转至最外层

        driver.switch_to.frame('main-frame')  # 跳转至商品列表frame
        time.sleep(2)

        # 搜索商品名称
        driver.find_element_by_name('keyword').send_keys("国王")
        time.sleep(2)
        driver.find_element_by_xpath('/html/body/div[1]/form/input[2]').click()

        time.sleep(2)
        # driver.find_element_by_xpath('//div[@id="listDiv"]/table[1]/tbody/tr[4]/td[1]/input').click()
        # time.sleep(2)
        # 删除文件
        driver.find_element_by_xpath('//div[@id="listDiv"]/table[1]/tbody/tr[4]/td[12]/a[4]/img').click()
        time.sleep(3)
        # 确定删除（点击警告框【确定】按钮）
        driver.switch_to.alert.accept()
        time.sleep(3)
        print("删除商品成功")

        # 关闭所有的窗口，并关闭浏览器驱动
        driver.quit()

if __name__ == '__main__':
    unittest.main()