'''引入需要调用的类和方法'''
import time
import unittest
from appium import webdriver

from page.page_tencent_channel import PageTencentChannel
from page.page_tencent_home import PageTencentHome


'''进入首页点击【频道按钮】'''
'''对不同的频道模块进行拖拽，交换位置'''
'''选择频道点击【成都】'''
'''点击【切换城市】按钮，切换城市'''

desired_capabilities ={
    'platformName':'Android',
    'deviceName':'127.0.0.1:62001',   #模拟器的名称
    'platformVersion':'7.1.2',   #设备的系统平台：Android、IOS
    'appPackage':'com.tencent.news',  #设备系统(模拟器)的Android版本号
    'appActivity':'com.tencent.news.activity.SplashActivity',
    'unicodeKeyboard':True,  #将输入内容设置为中文
    'resetKeyboard':True, #重置输入法为初始状态
    'noReset':True, #设置后APP不会回到初始化状态;避免重启
    'newCommandTimeout':60 #命令等待超时时间


}
class TestTencentchannel(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_capabilities)
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()

    def test_channel(self):
        '''进入首页点击频道按钮'''
        PageTencentHome(self.driver).ele_channel_button()
        '''进入频道后，进行拖拽、选择操作'''
        PageTencentChannel(self.driver).channel()

        time.sleep(2)




if __name__ == '__main__':
    unittest.main()

