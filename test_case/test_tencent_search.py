'''引入需要调用的类和方法'''
import unittest
from appium import webdriver
from page.page_tencent_home import PageTencentHome
from page.page_tencent_search import PageTencentSearch

'''进入首页，点击搜索框'''
'''输入搜索内容为：’华为手机‘'''
'''按下Enter按钮，确认搜索'''

desired_capabilities ={
    'platformName':'Android',
    'deviceName':'127.0.0.1:62001',   #模拟器的名称
    'platformVersion':'7.1.2',   #设备的系统平台：Android、IOS
    'appPackage':'com.tencent.news',  #设备系统(模拟器)的Android版本号
    'appActivity':'com.tencent.news.activity.SplashActivity',
    'unicodeKeyboard':True,  #将输入内容设置为中文
    'resetKeyboard':True, #重置输入法为初始状态
    'noReset':True, #设置后APP不会回到初始化状态;避免重启
    'newCommandTimeout':60 #命令等待超时时间


}
class TestTencentSearch(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_capabilities)
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()

    def test_search(self):
        '''进入首页进行刷新'''
        sx="360"
        sy="350"
        ex="360"
        ey="1440"
        time="500"
        PageTencentHome(self.driver).ele_Screen_refresh(sx,sy,ex,ey,time)
        '''输入搜索内容进行搜索'''
        content="华为手机"
        num="66"
        PageTencentSearch(self.driver).search(content,num)





if __name__ == '__main__':
    unittest.main()

