#引入unittest
import unittest

#引入time类
import time

#引入webdriver
from selenium import webdriver

class EcshopFront(unittest.TestCase):
    def test_registered(self):

        driver = webdriver.Chrome()

        driver.maximize_window()

        driver.get("http://192.168.1.241/hdshop ")

        time.sleep(2)

        driver.find_element_by_link_text("注册").click()

        driver.find_element_by_id("username").send_keys("mark14")

        driver.find_element_by_name("email").send_keys("832785414@qq.com")

        driver.find_element_by_name("password").send_keys("123456")

        driver.find_element_by_name("confirm_password").send_keys("123456")

        driver.find_element_by_name("extend_field2").send_keys("532785414")
        driver.find_element_by_name("extend_field3").send_keys("5263991")

        driver.find_element_by_name("extend_field4").send_keys("1898275414")

        driver.find_element_by_name("extend_field5").send_keys("13335678909")

        driver.find_element_by_name("sel_question").click()

        driver.find_element_by_xpath("//select[@name='sel_question']/option[2]").click()

        driver.find_element_by_name("passwd_answer").send_keys("19940426")

        driver.find_element_by_css_selector(".us_Submit_reg").click()
        print("注册成功")

        driver.quit()

    def test_login(self):

        driver = webdriver.Chrome()

        driver.maximize_window()

        driver.get("http://192.168.1.241/hdshop ")

        driver.find_element_by_link_text("登录").click()

        driver.find_element_by_name("username").send_keys("mark8")

        driver.find_element_by_name("password").send_keys("123456")

        driver.find_element_by_xpath("//input[@id='remember']").click()

        driver.find_element_by_name("submit").click()

        print("登录成功")

        time.sleep(3)

        driver.quit()

    def test_select(self):

        driver = webdriver.Chrome()

        driver.maximize_window()

        driver.get("http://192.168.1.241/hdshop ")
        time.sleep(2)

        driver.find_element_by_xpath("//div[@class='menu']/a[3]").click()
        time.sleep(2)

        driver.find_element_by_xpath("//div[@class='goodsItem']/a/img").click()
        print("查看商品成功")

        time.sleep(3)
        driver.quit()



if __name__ == '__main__':
    unittest.main()



