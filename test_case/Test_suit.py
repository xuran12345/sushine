import unittest

from test_case.Test_signin_front import SignInFront

from test_case.Test_login_front import  LogInFront

suit = unittest.TestSuite()
suit.addTest(SignInFront("test_e"))
suit.addTest(SignInFront("test_f"))
suit.addTest(LogInFront("test_d"))
suit.addTest(LogInFront("test_c"))

runner = unittest.TextTestRunner()

runner.run(suit)